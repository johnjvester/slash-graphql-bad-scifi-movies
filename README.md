# slash-graphql-bad-scifi-movies

> The `slash-graphql-bad-scifi-movies` repository is an [Angular](https://angular.io/) application 
> which utilizes [Slash GraphQL](https://dgraph.io/slash-graphql) for source data provided by Dgraph's 
> RESTful API. The actual source data is from [IMDB](http://www.imdb.com) and daily [file extractions](https://www.imdb.com/interfaces/) 
> which are provided in `tsv` format.  
> 
> The `tsv` files were processed by a simple [Java](https://en.wikipedia.org/wiki/Java_(programming_language)) 
> program (included in the `imdb` folder of this repository) to create a single file containing of all "Sci-Fi" movie 
> titles, plus the average rating and votes as noted by IMDB. The list was then reduced to create the lowest-rated Sci-Fi 
> (full-length) movies which have at least 500 votes.  Consider this a modern-day wish list for Mystery Science Theater 3000 
> ([MST3K](https://en.wikipedia.org/wiki/Mystery_Science_Theater_3000)).
> 
> The final list was narrowed down to the worst-rated 125 movies and was inserted into a `Movie` schema within 
> a free instance of Dgraph Slash GraphQL. A `User` schema was also created, housing a single user with references 
> to all the movies which had been viewed by that user.
> 
> The Angular application makes a RESTful API call to Dgraph Slash GraphQL to retrieve the list 
> of really bad Sci-Fi movies.  When the user clicks a row in the provided table, a modal appears 
> which includes the metadata from IMDB stored in GraphQL, plus the movie posted - provided by an API 
> call to [OMDb API](http://www.omdbapi.com/) and a free API key.  This modal also includes a button 
> called "Mark as Watched" - which is only enabled when the user's account does not include the provided 
> `Movie` in the list of movies which have been viewed.  (Please note - the "Mark as Watched" button has 
> no actual funtionality in this repository.)

## Publications

This repository is related to an article published on DZone.com:

* [Tracking the Worst Sci-Fi Movies with Angular and Slash GraphQL](https://dzone.com/articles/tracking-the-worst-sci-fi-movies-with-angular-and)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939

## Setup & Configuration

The following section walks through all the necessary steps to go from zero-to-full-results.  In cases where this is not truly 
required, comments will be included - along with alternative instructions.

### Getting Data From IMDB Interfaces

The following steps require the ability to run a Java (7 and above) program and some mechanism to filter a `tsv` text file. These 
steps are not required and a copy of an IMDB data set (with average rating and total vote count) has been included in this 
project in the `imdb\filtered.tsv` location.

* Visit the https://www.imdb.com/interfaces/ location
* Follow the steps to download a current copy of the `title.basics.tsv.gz` file
* Follow the steps to download a current copy of the `title.ratings.tsv.gz` file
* Extract both files into a working location (default values are shown below):
  * `/downloads/title.basics.tsv`
  * `/downloads/title.ratings.tsv`
* Open the `imdb\src\main\java\Application.java` file and make any necessary updates.
* Run the Java program, which will create the following file: (default value is included below)
  * `/downloads/filtered.tsv` (this file is included at the `imdb\filtered.tsv` location)
* The file should include the following columns:
  * `id`
  * `titleType`
  * `primaryTitle`
  * `originalTitle`
  * `isAdult`
  * `startYear`
  * `endYear`
  * `runtimeMinutes`
  * `genres`
  * `averageRating`
  * `numVotes`
* Open the `filtered.tsv` file in an application (e.g. Microsoft Excel) and perform any necessary filtering:
  * only `movie` value for the `titleType` column
  * remove any values where `isAdult` is greater than zero
  * only items which include "Sci-Fi" in the `genres` column
  * only items which have a value greater than or equal to 500 in the `numVotes` column
* Create a new data set which includes only the following columns:
   * `id`
   * `primaryTitle` (which will become `title`)
   * `startYear` (which will become `releaseYear`)
   * `runtimeMinutes`
   * `genres` (which will become `genre`)
   * `averageRating`
   * `numVotes` (which will become `votes`)
* Create a JSON data set (I used a new column in Microsoft Excel and the `CONCAT` method),  Below is a single example:
```
{id:"tt5311054", title:"Browncoats: Independence War", releaseYear:2015,runtimeMinutes:98,genre:"Action,Sci-Fi,War",averageRating:1.1,votes:717},
```

### Getting Started with Dgraph Slash GraphQL

Dgraph provides a free Slash GraphQL account which can be utilized by this repository and includes 10,000 credits.  To get started, simply launch the following URL:

https://slash.dgraph.io/

Once an account has been created, follow the steps listed below:

* Create a new backend and note the provided **GraphQL Endpoint** value in the Overview section of the Dgraph user interface
* Navigate to the **Develop | Schema** section of the Dgraph user interface
* Paste the following schema values:
```
type Movie {
    id: String! @id @search(by: [hash])
    title: String! @search(by: [fulltext])
    releaseYear: Int! @search
    runtimeMinutes: Int!
    genre: String! @search(by: [fulltext])
    averageRating: Float! @search
    votes: Int! @search
    seen: User
}

type User {
    username: String! @id @search(by: [hash])
    movies: [Movie] @hasInverse(field: seen)
}
```

At this point the GraphQL database is ready for use.  

Using the JSON data created in the "Getting Data From IMDB Interfaces" section (above), 
insert the `Movie` data into Dgraph Slash GraphQL.  Full example provided below:

```
mutation AddMovies {
  addMovie(input: [
    {id:"tt5311054", title:"Browncoats: Independence War", releaseYear:2015,runtimeMinutes:98,genre:"Action,Sci-Fi,War",averageRating:1.1,votes:717},
    {id:"tt2205589", title:"Rise of the Black Bat", releaseYear:2012,runtimeMinutes:80,genre:"Action,Sci-Fi",averageRating:1.2,votes:690},
    {id:"tt1854506", title:"Aliens vs. Avatars", releaseYear:2011,runtimeMinutes:80,genre:"Horror,Sci-Fi",averageRating:1.5,votes:1584},
    {id:"tt1360833", title:"Star Quest: The Odyssey", releaseYear:2009,runtimeMinutes:81,genre:"Sci-Fi",averageRating:1.5,votes:520},
    {id:"tt1196334", title:"After Last Season", releaseYear:2009,runtimeMinutes:93,genre:"Animation,Drama,Sci-Fi",averageRating:1.6,votes:7948},
    {id:"tt0358551", title:"Julie and Jack", releaseYear:2003,runtimeMinutes:90,genre:"Drama,Romance,Sci-Fi",averageRating:1.7,votes:542},
    {id:"tt0808240", title:"Turks in Space", releaseYear:2006,runtimeMinutes:110,genre:"Action,Comedy,Sci-Fi",averageRating:1.7,votes:15611},
    {id:"tt1023347", title:"Universal Soldiers", releaseYear:2007,runtimeMinutes:85,genre:"Action,Adventure,Sci-Fi",averageRating:1.7,votes:1449},
    {id:"tt7963218", title:"Atlantic Rim: Resurrection", releaseYear:2018,runtimeMinutes:86,genre:"Action,Adventure,Sci-Fi",averageRating:1.8,votes:719},
    {id:"tt0457342", title:"Evil Behind You", releaseYear:2006,runtimeMinutes:90,genre:"Action,Comedy,Sci-Fi",averageRating:1.8,votes:596},
    {id:"tt2385001", title:"Android Insurrection", releaseYear:2012,runtimeMinutes:78,genre:"Action,Sci-Fi",averageRating:1.8,votes:573},
    {id:"tt0107715", title:"Nukie", releaseYear:1987,runtimeMinutes:95,genre:"Family,Sci-Fi",averageRating:1.8,votes:1121},
    {id:"tt0105680", title:"Ultra Warrior", releaseYear:1990,runtimeMinutes:81,genre:"Action,Adventure,Sci-Fi",averageRating:1.9,votes:695},
    {id:"tt1887676", title:"Battle: New York, Day 2", releaseYear:2011,runtimeMinutes:94,genre:"Action,Sci-Fi,Thriller",averageRating:1.9,votes:705},
    {id:"tt0270846", title:"Superbabies: Baby Geniuses 2", releaseYear:2004,runtimeMinutes:88,genre:"Comedy,Family,Sci-Fi",averageRating:1.9,votes:29641},
    {id:"tt0086026", title:"Extra Terrestrial Visitors", releaseYear:1983,runtimeMinutes:80,genre:"Fantasy,Horror,Sci-Fi",averageRating:2,votes:4204},
    {id:"tt0061191", title:"The Wild World of Batwoman", releaseYear:1966,runtimeMinutes:70,genre:"Comedy,Sci-Fi",averageRating:2,votes:4243},
    {id:"tt1910498", title:"Alien Armageddon", releaseYear:2011,runtimeMinutes:95,genre:"Sci-Fi",averageRating:2,votes:1158},
    {id:"tt4421344", title:"Game Therapy", releaseYear:2015,runtimeMinutes:97,genre:"Action,Comedy,Sci-Fi",averageRating:2.1,votes:713},
    {id:"tt1916763", title:"Zombex", releaseYear:2013,runtimeMinutes:81,genre:"Horror,Sci-Fi,Thriller",averageRating:2.1,votes:630},
    {id:"tt0367677", title:"Dracula 3000", releaseYear:2004,runtimeMinutes:86,genre:"Horror,Sci-Fi",averageRating:2.1,votes:5946},
    {id:"tt1557843", title:"Purge", releaseYear:2010,runtimeMinutes:80,genre:"Drama,Sci-Fi",averageRating:2.1,votes:1330},
    {id:"tt0231138", title:"Ancient Evil: Scream of the Mummy", releaseYear:1999,runtimeMinutes:86,genre:"Horror,Sci-Fi",averageRating:2.1,votes:1163},
    {id:"tt1704201", title:"Parasitic", releaseYear:2012,runtimeMinutes:79,genre:"Horror,Sci-Fi",averageRating:2.1,votes:647},
    {id:"tt0048256", title:"King Dinosaur", releaseYear:1955,runtimeMinutes:63,genre:"Adventure,Sci-Fi",averageRating:2.1,votes:1217},
    {id:"tt5207004", title:"Battalion", releaseYear:2018,runtimeMinutes:94,genre:"Action,Drama,Sci-Fi",averageRating:2.1,votes:525},
    {id:"tt0055562", title:"Invasion of the Neptune Men", releaseYear:1961,runtimeMinutes:75,genre:"Action,Sci-Fi",averageRating:2.2,votes:2457},
    {id:"tt0093405", title:"Leonard Part 6", releaseYear:1987,runtimeMinutes:85,genre:"Action,Comedy,Sci-Fi",averageRating:2.2,votes:7834},
    {id:"tt0054673", title:"The Beast of Yucca Flats", releaseYear:1961,runtimeMinutes:54,genre:"Horror,Sci-Fi",averageRating:2.2,votes:7982},
    {id:"tt2547942", title:"Starship: Rising", releaseYear:2014,runtimeMinutes:91,genre:"Action,Sci-Fi",averageRating:2.2,votes:839},
    {id:"tt0059464", title:"Monster a Go-Go", releaseYear:1965,runtimeMinutes:70,genre:"Horror,Sci-Fi",averageRating:2.2,votes:7924},
    {id:"tt0082410", title:"Frankenstein Island", releaseYear:1981,runtimeMinutes:97,genre:"Horror,Sci-Fi",averageRating:2.2,votes:893},
    {id:"tt1131742", title:"Mutants", releaseYear:2008,runtimeMinutes:83,genre:"Action,Horror,Sci-Fi",averageRating:2.2,votes:553},
    {id:"tt2275499", title:"Alien Dawn", releaseYear:2012,runtimeMinutes:86,genre:"Action,Sci-Fi,Thriller",averageRating:2.3,votes:873},
    {id:"tt0089280", title:"Hobgoblins", releaseYear:1988,runtimeMinutes:88,genre:"Comedy,Horror,Sci-Fi",averageRating:2.3,votes:10435},
    {id:"tt0075343", title:"Track of the Moon Beast", releaseYear:1976,runtimeMinutes:90,genre:"Horror,Sci-Fi",averageRating:2.3,votes:3283},
    {id:"tt8783146", title:"Jurassic Galaxy", releaseYear:2018,runtimeMinutes:74,genre:"Action,Adventure,Sci-Fi",averageRating:2.3,votes:562},
    {id:"tt0093872", title:"Robot Holocaust", releaseYear:1987,runtimeMinutes:79,genre:"Sci-Fi",averageRating:2.3,votes:1592},
    {id:"tt0098048", title:"Gor II", releaseYear:1988,runtimeMinutes:89,genre:"Action,Fantasy,Sci-Fi",averageRating:2.3,votes:1523},
    {id:"tt0060074", title:"Agent for H.A.R.M.", releaseYear:1966,runtimeMinutes:84,genre:"Action,Adventure,Sci-Fi",averageRating:2.3,votes:1218},
    {id:"tt0251005", title:"Carnivore", releaseYear:2000,runtimeMinutes:80,genre:"Horror,Sci-Fi",averageRating:2.3,votes:534},
    {id:"tt0053464", title:"Prince of Space", releaseYear:1959,runtimeMinutes:121,genre:"Action,Adventure,Sci-Fi",averageRating:2.3,votes:2726},
    {id:"tt2081438", title:"Zombie Massacre", releaseYear:2013,runtimeMinutes:87,genre:"Action,Horror,Sci-Fi",averageRating:2.3,votes:1699},
    {id:"tt0369226", title:"Alone in the Dark", releaseYear:2005,runtimeMinutes:99,genre:"Action,Horror,Sci-Fi",averageRating:2.4,votes:41552},
    {id:"tt0046977", title:"Fire Maidens of Outer Space", releaseYear:1956,runtimeMinutes:80,genre:"Sci-Fi",averageRating:2.4,votes:1485},
    {id:"tt0145529", title:"Time Chasers", releaseYear:1994,runtimeMinutes:89,genre:"Sci-Fi",averageRating:2.4,votes:3051},
    {id:"tt3481210", title:"2047: Sights of Death", releaseYear:2014,runtimeMinutes:89,genre:"Action,Sci-Fi,Thriller",averageRating:2.4,votes:1379},
    {id:"tt0183072", title:"Feeders", releaseYear:1996,runtimeMinutes:69,genre:"Drama,Horror,Sci-Fi",averageRating:2.4,votes:746},
    {id:"tt0088925", title:"City Limits", releaseYear:1984,runtimeMinutes:86,genre:"Action,Adventure,Sci-Fi",averageRating:2.4,votes:1192},
    {id:"tt2439946", title:"40 Days and Nights", releaseYear:2012,runtimeMinutes:86,genre:"Action,Adventure,Sci-Fi",averageRating:2.4,votes:2001},
    {id:"tt1918886", title:"Joker", releaseYear:2012,runtimeMinutes:104,genre:"Comedy,Family,Sci-Fi",averageRating:2.4,votes:5012},
    {id:"tt2536086", title:"Quarantine L.A.", releaseYear:2013,runtimeMinutes:75,genre:"Horror,Sci-Fi",averageRating:2.4,votes:525},
    {id:"tt0115506", title:"Alien Species", releaseYear:1996,runtimeMinutes:92,genre:"Sci-Fi",averageRating:2.5,votes:529},
    {id:"tt0062035", title:"Danger!! Death Ray", releaseYear:1967,runtimeMinutes:93,genre:"Adventure,Sci-Fi",averageRating:2.5,votes:643},
    {id:"tt0072666", title:"Zaat", releaseYear:1971,runtimeMinutes:100,genre:"Horror,Sci-Fi",averageRating:2.5,votes:4636},
    {id:"tt0065470", title:"Bigfoot", releaseYear:1970,runtimeMinutes:84,genre:"Horror,Sci-Fi,Thriller",averageRating:2.5,votes:736},
    {id:"tt0061592", title:"Doomsday Machine", releaseYear:1972,runtimeMinutes:83,genre:"Sci-Fi",averageRating:2.5,votes:957},
    {id:"tt3501492", title:"Rz-9", releaseYear:2015,runtimeMinutes:90,genre:"Action,Sci-Fi",averageRating:2.5,votes:584},
    {id:"tt0426396", title:"Alien Abduction", releaseYear:2005,runtimeMinutes:96,genre:"Horror,Sci-Fi,Thriller",averageRating:2.5,votes:1253},
    {id:"tt0270393", title:"Groom Lake", releaseYear:2002,runtimeMinutes:92,genre:"Horror,Sci-Fi",averageRating:2.5,votes:619},
    {id:"tt0475970", title:"Creature of Darkness", releaseYear:2009,runtimeMinutes:81,genre:"Horror,Sci-Fi",averageRating:2.5,votes:724},
    {id:"tt0050717", title:"The Aztec Mummy Against the Humanoid Robot", releaseYear:1958,runtimeMinutes:65,genre:"Horror,Sci-Fi",averageRating:2.5,votes:2394},
    {id:"tt3312936", title:"LA Apocalypse", releaseYear:2015,runtimeMinutes:80,genre:"Action,Sci-Fi,Thriller",averageRating:2.5,votes:654},
    {id:"tt0116839", title:"Lawnmower Man 2: Beyond Cyberspace", releaseYear:1996,runtimeMinutes:93,genre:"Action,Sci-Fi,Thriller",averageRating:2.5,votes:8889},
    {id:"tt0185183", title:"Battlefield Earth", releaseYear:2000,runtimeMinutes:118,genre:"Action,Adventure,Sci-Fi",averageRating:2.5,votes:75675},
    {id:"tt0272425", title:"Reptile 2001", releaseYear:1999,runtimeMinutes:99,genre:"Action,Fantasy,Sci-Fi",averageRating:2.6,votes:1567},
    {id:"tt4290974", title:"Taking Earth", releaseYear:2017,runtimeMinutes:100,genre:"Sci-Fi",averageRating:2.6,votes:1038},
    {id:"tt0088134", title:"Slapstick of Another Kind", releaseYear:1982,runtimeMinutes:82,genre:"Comedy,Fantasy,Sci-Fi",averageRating:2.6,votes:739},
    {id:"tt0088380", title:"Warrior of the Lost World", releaseYear:1984,runtimeMinutes:92,genre:"Action,Sci-Fi,Thriller",averageRating:2.6,votes:2427},
    {id:"tt9562694", title:"Alien Warfare", releaseYear:2019,runtimeMinutes:88,genre:"Action,Sci-Fi",averageRating:2.6,votes:2757},
    {id:"tt3233972", title:"Another", releaseYear:2014,runtimeMinutes:80,genre:"Fantasy,Horror,Sci-Fi",averageRating:2.6,votes:510},
    {id:"tt0082815", title:"Night of the Zombies", releaseYear:1981,runtimeMinutes:88,genre:"Horror,Sci-Fi",averageRating:2.6,votes:553},
    {id:"tt0102569", title:"A Nymphoid Barbarian in Dinosaur Hell", releaseYear:1990,runtimeMinutes:82,genre:"Fantasy,Horror,Sci-Fi",averageRating:2.6,votes:1458},
    {id:"tt11385066", title:"The App", releaseYear:2019,runtimeMinutes:78,genre:"Drama,Sci-Fi,Thriller",averageRating:2.6,votes:1087},
    {id:"tt0366985", title:"Seed", releaseYear:2004,runtimeMinutes:143,genre:"Action,Fantasy,Sci-Fi",averageRating:2.6,votes:1100},
    {id:"tt0097781", title:"Lords of the Deep", releaseYear:1989,runtimeMinutes:77,genre:"Sci-Fi,Thriller",averageRating:2.6,votes:945},
    {id:"tt0095738", title:"Nightfall", releaseYear:1988,runtimeMinutes:83,genre:"Mystery,Sci-Fi",averageRating:2.7,votes:623},
    {id:"tt4073890", title:"Andron", releaseYear:2015,runtimeMinutes:100,genre:"Action,Sci-Fi",averageRating:2.7,votes:2805},
    {id:"tt0056499", title:"The Slime People", releaseYear:1963,runtimeMinutes:76,genre:"Horror,Sci-Fi",averageRating:2.7,votes:1103},
    {id:"tt0918554", title:"Plaguers", releaseYear:2008,runtimeMinutes:86,genre:"Horror,Sci-Fi",averageRating:2.7,votes:837},
    {id:"tt0390463", title:"Sci-Fighter", releaseYear:2004,runtimeMinutes:90,genre:"Action,Sci-Fi",averageRating:2.7,votes:572},
    {id:"tt0062036", title:"Night Fright", releaseYear:1967,runtimeMinutes:75,genre:"Horror,Sci-Fi",averageRating:2.7,votes:734},
    {id:"tt0106620", title:"The Crawlers", releaseYear:1993,runtimeMinutes:94,genre:"Horror,Sci-Fi,Thriller",averageRating:2.7,votes:1116},
    {id:"tt1056467", title:"Dying God", releaseYear:2008,runtimeMinutes:125,genre:"Horror,Sci-Fi,Thriller",averageRating:2.7,votes:990},
    {id:"tt4530832", title:"Road Wars", releaseYear:2015,runtimeMinutes:90,genre:"Action,Sci-Fi",averageRating:2.7,votes:682},
    {id:"tt2324928", title:"Alienate", releaseYear:2016,runtimeMinutes:90,genre:"Drama,Horror,Sci-Fi",averageRating:2.7,votes:738},
    {id:"tt0079573", title:"Monstroid", releaseYear:1980,runtimeMinutes:98,genre:"Horror,Sci-Fi",averageRating:2.7,votes:609},
    {id:"tt3530690", title:"10,000 Days", releaseYear:2014,runtimeMinutes:91,genre:"Sci-Fi",averageRating:2.7,votes:689},
    {id:"tt0046066", title:"Mesa of Lost Women", releaseYear:1953,runtimeMinutes:70,genre:"Horror,Sci-Fi",averageRating:2.7,votes:1377},
    {id:"tt0068291", title:"Blood of Ghastly Horror", releaseYear:1967,runtimeMinutes:85,genre:"Horror,Sci-Fi",averageRating:2.7,votes:547},
    {id:"tt0098156", title:"R.O.T.O.R.", releaseYear:1987,runtimeMinutes:90,genre:"Action,Sci-Fi,Thriller",averageRating:2.7,votes:2830},
    {id:"tt1509125", title:"Virus X", releaseYear:2010,runtimeMinutes:85,genre:"Horror,Sci-Fi",averageRating:2.7,votes:549},
    {id:"tt0102187", title:"Karate Cop", releaseYear:1991,runtimeMinutes:90,genre:"Action,Sci-Fi",averageRating:2.7,votes:617},
    {id:"tt1993396", title:"Toxin", releaseYear:2015,runtimeMinutes:77,genre:"Drama,Horror,Sci-Fi",averageRating:2.7,votes:1017},
    {id:"tt0112320", title:"Alien Terminator", releaseYear:1995,runtimeMinutes:80,genre:"Horror,Sci-Fi",averageRating:2.7,votes:514},
    {id:"tt0077640", title:"War of the Robots", releaseYear:1978,runtimeMinutes:103,genre:"Adventure,Sci-Fi",averageRating:2.7,votes:694},
    {id:"tt0077834", title:"Laserblast", releaseYear:1978,runtimeMinutes:85,genre:"Horror,Sci-Fi",averageRating:2.7,votes:5641},
    {id:"tt0130633", title:"Body of the Prey", releaseYear:1967,runtimeMinutes:93,genre:"Horror,Sci-Fi",averageRating:2.8,votes:515},
    {id:"tt2095742", title:"Paranormal Incident", releaseYear:2011,runtimeMinutes:82,genre:"Horror,Sci-Fi",averageRating:2.8,votes:824},
    {id:"tt3626436", title:"Mutant World", releaseYear:2014,runtimeMinutes:82,genre:"Sci-Fi",averageRating:2.8,votes:644},
    {id:"tt0101264", title:"Abraxas, Guardian of the Universe", releaseYear:1990,runtimeMinutes:90,genre:"Action,Sci-Fi",averageRating:2.8,votes:1730},
    {id:"tt0052286", title:"Terror from the Year 5000", releaseYear:1958,runtimeMinutes:66,genre:"Sci-Fi",averageRating:2.8,votes:1214},
    {id:"tt2564978", title:"Lost Time", releaseYear:2014,runtimeMinutes:93,genre:"Horror,Sci-Fi,Thriller",averageRating:2.8,votes:1007},
    {id:"tt0080346", title:"The Alien Dead", releaseYear:1980,runtimeMinutes:74,genre:"Comedy,Horror,Sci-Fi",averageRating:2.8,votes:911},
    {id:"tt0070910", title:"Wam Bam Thank You Spaceman", releaseYear:1975,runtimeMinutes:75,genre:"Comedy,Sci-Fi",averageRating:2.8,votes:528},
    {id:"tt2721152", title:"P-51 Dragon Fighter", releaseYear:2014,runtimeMinutes:85,genre:"Action,Fantasy,Sci-Fi",averageRating:2.8,votes:588},
    {id:"tt3133718", title:"Pink Zone", releaseYear:2014,runtimeMinutes:90,genre:"Drama,Romance,Sci-Fi",averageRating:2.8,votes:529},
    {id:"tt5242684", title:"Dance to Death", releaseYear:2017,runtimeMinutes:90,genre:"Action,Drama,Sci-Fi",averageRating:2.8,votes:787},
    {id:"tt1622991", title:"Infected", releaseYear:2013,runtimeMinutes:95,genre:"Action,Horror,Sci-Fi",averageRating:2.8,votes:1062},
    {id:"tt0078317", title:"Star Odyssey", releaseYear:1979,runtimeMinutes:103,genre:"Sci-Fi",averageRating:2.8,votes:506},
    {id:"tt1666187", title:"Jurassic Predator: Xtinction", releaseYear:2014,runtimeMinutes:90,genre:"Action,Horror,Sci-Fi",averageRating:2.8,votes:791},
    {id:"tt2518926", title:"Age of Dinosaurs", releaseYear:2013,runtimeMinutes:88,genre:"Action,Adventure,Sci-Fi",averageRating:2.8,votes:2230},
    {id:"tt0265198", title:"Gangland", releaseYear:2001,runtimeMinutes:90,genre:"Action,Sci-Fi",averageRating:2.8,votes:539},
    {id:"tt0481509", title:"Displaced", releaseYear:2006,runtimeMinutes:99,genre:"Action,Adventure,Sci-Fi",averageRating:2.8,votes:733},
    {id:"tt0346811", title:"The Curse of the Komodo", releaseYear:2004,runtimeMinutes:92,genre:"Adventure,Horror,Sci-Fi",averageRating:2.8,votes:1423},
    {id:"tt0278259", title:".com for Murder", releaseYear:2002,runtimeMinutes:96,genre:"Horror,Sci-Fi,Thriller",averageRating:2.8,votes:2127},
    {id:"tt2620490", title:"Army of Frankensteins", releaseYear:2013,runtimeMinutes:108,genre:"Adventure,Horror,Sci-Fi",averageRating:2.8,votes:536},
    {id:"tt0923653", title:"The Dead Undead", releaseYear:2010,runtimeMinutes:89,genre:"Action,Horror,Sci-Fi",averageRating:2.9,votes:823},
    {id:"tt0149677", title:"War of the Planets", releaseYear:1977,runtimeMinutes:89,genre:"Adventure,Sci-Fi",averageRating:2.9,votes:762},
    {id:"tt1564369", title:"Scavengers", releaseYear:2013,runtimeMinutes:94,genre:"Action,Sci-Fi,Thriller",averageRating:2.9,votes:925},
    {id:"tt0177886", title:"The Killer Eye", releaseYear:1999,runtimeMinutes:72,genre:"Comedy,Horror,Sci-Fi",averageRating:2.9,votes:880},
    {id:"tt0100116", title:"Martians Go Home", releaseYear:1989,runtimeMinutes:89,genre:"Comedy,Sci-Fi",averageRating:2.9,votes:671},
    {id:"tt0110022", title:"The Hidden II", releaseYear:1993,runtimeMinutes:91,genre:"Crime,Horror,Sci-Fi",averageRating:2.9,votes:1199},
    {id:"tt0068313", title:"Brain of Blood", releaseYear:1971,runtimeMinutes:87,genre:"Horror,Sci-Fi",averageRating:2.9,votes:727},
    {id:"tt1754438", title:"Robotropolis", releaseYear:2011,runtimeMinutes:85,genre:"Action,Adventure,Sci-Fi",averageRating:2.9,votes:1180}
  ])
}
```

Next, `User` data needs to be inserted.  For the purposes of this repository, a single user (with a `username` value of `johnjvester`) will be 
inserted with links to the movies which have been viewed.  Below, is a fully-functional example:

```
mutation AddUser {
  addUser(input: 
    [
      {
        username: "johnjvester", 
        movies: [
          {id: "tt0052286"}, 
          {id: "tt0077834"}, 
          {id: "tt0145529"}, 
          {id: "tt0053464"}, 
          {id: "tt0060074"}, 
          {id: "tt0075343"}, 
          {id: "tt0089280"}, 
          {id: "tt0059464"}, 
          {id: "tt0055562"}
        ]
      }
    ]) {
    numUids
  }
}
```
At this point, the new backend service is available for use at the **GraphQL Endpoint** noted in the **Overview** section of the Dgraph interface.

### Getting Started with OMDb API

Having the poster for a given movie title makes the modal screen look a bit more polished.  The OMDb API allows for items to be queried using the 
same unique key which is used by IMDB.  

In order to show the movie poster in the Angular application, a free OMDb API key is required:

1. Visit the http://www.omdbapi.com/apikey.aspx to request an API key
2. Select the FREE option and provide an email address
3. Single-click the **Submit** button and follow any required follow-up steps
4. Note the **Here is your key** value provided via email from The OMDb API.

### Setting Up Angular

The last step is to configure the Angular application to connect to Dgraph Slash GraphQL and OMDb API. 

The `environment.ts` and `environmant.prod.ts` files need to be updated for the Angular application to function properly. The 
only attributes which need to be updated are the `api` (which is the **GraphQL Endpoint** value noted above) and `omdbKey` (which is the key value noted in the OMDb email) attributes.

Below, is an example from the `environment.ts` file:

```
export const environment = {
  production: false,
  api: 'https://some-host-instance.us-west-2.aws.cloud.dgraph.io/graphql',
  omdbApi: 'http://www.omdbapi.com/?apikey=',
  omdbKey: 'omdbApiKeyGoesHere'
};
```  

Both updates should include the `'` markers to store these properties as a `string`.  Updates to the `production` and `omdbApi` properties should not be necessary.

> please note - if a different user (other than `johnjvester`) was created in Slash GraphQL, the `view-movie-modal.component.ts` needs to be updated to reflect the correct `username` value.

After running `npm ci` (or `npm install`) within this repository, the Angular application can be started using the `ng serve` command.

## Using the Application

With the Angular application running and the Slash GraphQL running, the following screen should be displayed:

![Angular Client Example](./BadMoviesView.png)

Single-clicking an item on the list for a movie not seen by the `johnjvester` user appears as shown below:

![Angular Client Example](./BadMovieNotSeen.png)

Notice the **Mark as Watched** button is active.

Single-clicking an item on the list for a movie in which `johnjvester` has watched appears as shown below:

![Angular Client Example](./BadMovieSeen.png)

Notice the **Mark as Watched** button is not active, since this movie has already been seen.


Made with ♥ by johnjvester@gmail.com, because I enjoy writing code.
