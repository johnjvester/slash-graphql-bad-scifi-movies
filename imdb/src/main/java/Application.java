import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Application {
    private static final String DEFAULT_GENRE = "Sci-Fi";
    private static final String USER_HOME = "user.home";
    private static final String DELIMITER = "\t";

    private static final String TITLE_BASICS_TSV_FILE_LOCATION = "/downloads/title.basics.tsv";
    private static final String TITLE_RATINGS_FILE_LOCATION = "/downloads/title.ratings.tsv";

    private static final String DESTINATION_FILE = "/downloads/filtered.tsv";

    public static void main(String[] args) throws IOException {
        String genre = DEFAULT_GENRE;

        if (args != null && args.length > 0) {
            genre = args[0];
        }

        Collection<String> data = filterData(TITLE_BASICS_TSV_FILE_LOCATION, genre);

        if (CollectionUtils.isNotEmpty(data)) {
            writeFile(data, DESTINATION_FILE);
        }
    }

    private static Collection<String> filterData(String fileName, String genre) throws IOException {
        Map<String, String> data = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(System.getProperty(USER_HOME) + fileName))) {
            String string;
            long lineNumber = 0;

            while ((string = br.readLine()) != null) {
                if (lineNumber > 0 && StringUtils.contains(string, genre)) {
                    String firstItem = StringUtils.substringBefore(string, DELIMITER);
                    data.put(firstItem, string);
                }

                logResults(lineNumber, fileName);
                lineNumber++;
            }

            if (MapUtils.isNotEmpty(data)) {
                appendUserRatings(data, TITLE_RATINGS_FILE_LOCATION);
            }
        }

        return data.values();
    }

    private static void appendUserRatings(Map<String, String> data, String fileName) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(System.getProperty(USER_HOME) + fileName))) {
            String string;
            long lineNumber = 0;

            while ((string = br.readLine()) != null) {
                if (lineNumber > 0) {
                    String firstItem = StringUtils.substringBefore(string, DELIMITER);
                    if (data.containsKey(firstItem)) {
                        data.put(firstItem, data.get(firstItem) + DELIMITER + StringUtils.substringAfter(string, DELIMITER));
                    }
                }

                logResults(lineNumber, fileName);
                lineNumber++;
            }
        }
    }

    private static void writeFile(Collection<String> data, String fileName) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(System.getProperty(USER_HOME) + fileName))) {
            for (String str : data) {
                bw.write(str);
                bw.newLine();
            }
        }
    }

    private static void logResults(long lineNumber, String fileName) {
        if (lineNumber % 10000 == 0) {
            System.out.println("Completed " + lineNumber + " " + fileName + " records");
        }
    }
}
