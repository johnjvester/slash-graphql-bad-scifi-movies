export const environment = {
  production: true,
  api: 'https://some-host-instance.us-west-2.aws.cloud.dgraph.io/graphql',
  omdbApi: 'http://www.omdbapi.com/?apikey=',
  omdbKey: 'omdbApiKeyGoesHere'
};
