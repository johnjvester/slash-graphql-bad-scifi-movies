import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, tap} from 'rxjs/operators';
import ErrorUtils from '../utils/error-utils';

@Injectable({
  providedIn: 'root'
})
export class OmdbService {

  constructor(private http: HttpClient) { }
  baseUrl: string = environment.omdbApi + environment.omdbKey;

  getMoviePoster(id:string) {
    return this.http.get<any>(this.baseUrl + '&i=' + id).pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }
}
