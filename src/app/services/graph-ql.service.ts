import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, tap} from 'rxjs/operators';
import ErrorUtils from '../utils/error-utils';
import {QueryMovieResponse} from '../models/query-movie-response';
import {GetUserResponse} from '../models/get-user-response';

@Injectable({
  providedIn: 'root'
})
export class GraphQLService {
  allMovies:string = '{queryMovie(filter: {}) {votes, title, runtimeMinutes, releaseYear, id, genre, averageRating}}';
  singleUserPrefix:string = '{getUser(username:"';
  singleUserSuffix:string = '"){username,movies{title,id}}}';

  constructor(private http: HttpClient) { }
  baseUrl: string = environment.api;

  getMovies() {
    return this.http.get<QueryMovieResponse>(this.baseUrl + '?query=' + this.allMovies).pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }

  getUser(username:string) {
    return this.http.get<GetUserResponse>(this.baseUrl + '?query=' + this.singleUserPrefix + username + this.singleUserSuffix).pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }

}
