import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {StarRatingModule} from 'angular-star-rating';
import { NgbdSortableHeaderDirective } from './directives/ngbd-sortable-header.directive';
import { ViewMovieModalComponent } from './view-movie-modal/view-movie-modal.component';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    NgbdSortableHeaderDirective,
    ViewMovieModalComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    StarRatingModule.forRoot(),
    FormsModule,
    NgbModule
  ],
  providers: [],
  entryComponents: [
    ViewMovieModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
