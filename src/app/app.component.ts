import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {GraphQLService} from './services/graph-ql.service';
import {Movie} from './models/movie';
import {QueryMovieResponse} from './models/query-movie-response';
import {NgbdSortableHeaderDirective, SortColumn, SortDirection} from './directives/ngbd-sortable-header.directive';
import {ViewMovieModalComponent} from './view-movie-modal/view-movie-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: SortColumn;
  direction: SortDirection;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'slash-graphql-bad-scifi-movies';
  movies: Movie[];

  constructor(private graphQlService: GraphQLService, public modalService: NgbModal) { }

  ngOnInit() {
    this.graphQlService.getMovies()
      .subscribe(data => {
        if (data) {
          let queryMovieResponse: QueryMovieResponse = data;
          this.movies = queryMovieResponse.data.queryMovie;
          this.movies.sort((a, b) => (a.title > b.title) ? 1 : -1)
        }
      }, (error) => {
        console.error('error', error);
      }).add(() => {
    });
  }

  @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    if (direction !== '' && column !== '') {
      this.movies = [...this.movies].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }

  showMovie(thisMovie:Movie) {
    const modalRef = this.modalService.open(ViewMovieModalComponent, {size: 'lg'});
    modalRef.componentInstance.movie = thisMovie;

    modalRef.result.then(movie => {
      // no-op
    }, (error) => {
      // no-op
    });
  }
}
