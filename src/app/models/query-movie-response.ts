import {QueryMovie} from './query-movie';

export class QueryMovieResponse {
  data: QueryMovie;
  extensions: any;
}
