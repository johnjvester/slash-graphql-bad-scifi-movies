import {Movie} from './movie';

export class User {
  username:string;
  movies:Movie[];
}
