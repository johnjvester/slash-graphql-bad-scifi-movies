export class Movie {
  id:string;
  title:string;
  releaseYear:number;
  runtimeMinutes:number;
  genre:string;
  averageRating:number;
  votes:number;
}
