import {GetUser} from './get-user';

export class GetUserResponse {
  data: GetUser;
  extensions: any;
}
