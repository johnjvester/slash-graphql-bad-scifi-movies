import {Component, OnInit} from '@angular/core';
import {Movie} from '../models/movie';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {OmdbService} from '../services/omdb.service';
import {User} from '../models/user';
import {GraphQLService} from '../services/graph-ql.service';

@Component({
  selector: 'app-view-movie-modal',
  templateUrl: './view-movie-modal.component.html',
  styleUrls: ['./view-movie-modal.component.css']
})
export class ViewMovieModalComponent implements OnInit {
  posterUrl: string;
  movie: Movie;
  user: User;
  username: string = 'johnjvester';
  seenThisMovie: boolean = false;

  constructor(public activeModal: NgbActiveModal, public graphQlService: GraphQLService, public omdbService: OmdbService) { }

  ngOnInit() {
    if (this.movie && this.movie.id) {
      this.omdbService.getMoviePoster(this.movie.id)
        .subscribe(data => {
          if (data && data.Poster) {
            this.posterUrl = data.Poster;

            this.graphQlService.getUser(this.username)
              .subscribe(getUserResponse => {
                if (getUserResponse && getUserResponse.data && getUserResponse.data.getUser) {
                  this.user = getUserResponse.data.getUser;
                  this.hasSeenThisMovie();
                }
              }, (error) => {
                console.error('error', error);
              }).add(() => {
            });
          }
        }, (error) => {
          console.error('error', error);
        }).add(() => {
      });
    }
  }

  private hasSeenThisMovie() {
    if (this.user && this.user.movies && this.movie) {
      for (let movie of this.user.movies) {
        if (movie.id === this.movie.id) {
          this.seenThisMovie = true;
          return;
        }
      }
    }

    this.seenThisMovie = false;
  }
}
